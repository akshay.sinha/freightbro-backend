define({
  "name": "Freightbro API",
  "version": "0.1.0",
  "description": "Basic REST API docs",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-05-21T18:10:25.219Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
