define({ "api": [
  {
    "type": "post",
    "url": "/login",
    "title": "User login",
    "group": "Login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Login successful</p>"
          },
          {
            "group": "200",
            "type": "JWT",
            "optional": false,
            "field": "token",
            "description": "<p>JSON Web token</p>"
          },
          {
            "group": "200",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>You are successfully logged in!</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/users.js",
    "groupTitle": "Login",
    "name": "PostLogin"
  },
  {
    "type": "delete",
    "url": "*",
    "title": "DELETE methods",
    "group": "Mock_Server",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/mock.js",
    "groupTitle": "Mock_Server",
    "name": "Delete"
  },
  {
    "type": "get",
    "url": "*",
    "title": "GET methods",
    "group": "Mock_Server",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/mock.js",
    "groupTitle": "Mock_Server",
    "name": "Get"
  },
  {
    "type": "POST",
    "url": "*",
    "title": "POST methods",
    "group": "Mock_Server",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/mock.js",
    "groupTitle": "Mock_Server",
    "name": "Post"
  },
  {
    "type": "put",
    "url": "*",
    "title": "PUT methods",
    "group": "Mock_Server",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/mock.js",
    "groupTitle": "Mock_Server",
    "name": "Put"
  },
  {
    "type": "get",
    "url": "/project",
    "title": "Project Details",
    "group": "Project",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "projects",
            "description": "<p>Projects Object</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/projects.js",
    "groupTitle": "Project",
    "name": "GetProject"
  },
  {
    "type": "post",
    "url": "/adduser/:projectName",
    "title": "New Project Method",
    "group": "Project",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>Url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Method Type</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "requestBody",
            "description": "<p>Sample Request</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "optionalHeaders",
            "description": "<p>Header</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Updated",
            "description": "<p>Updated</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Project not found!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/projects.js",
    "groupTitle": "Project",
    "name": "PostAdduserProjectname"
  },
  {
    "type": "post",
    "url": "/project",
    "title": "User login",
    "group": "Project",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "owner",
            "description": "<p>Project Owner</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "project",
            "description": "<p>Project Object</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/projects.js",
    "groupTitle": "Project",
    "name": "PostProject"
  },
  {
    "type": "post",
    "url": "/project/:name",
    "title": "New Project Method",
    "group": "Project",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>Url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Method Type</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "requestBody",
            "description": "<p>Sample Request</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "optionalHeaders",
            "description": "<p>Header</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Updated",
            "description": "<p>Updated</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Project not found!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/projects.js",
    "groupTitle": "Project",
    "name": "PostProjectName"
  },
  {
    "type": "put",
    "url": "/project/:name",
    "title": "Project Method update",
    "group": "Project",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>Url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Method Type</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "requestBody",
            "description": "<p>Sample Request</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "responseBody",
            "description": "<p>Sample Response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "optionalHeaders",
            "description": "<p>Header</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Updated",
            "description": "<p>Updated</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Project not found!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/projects.js",
    "groupTitle": "Project",
    "name": "PutProjectName"
  },
  {
    "type": "post",
    "url": "/signup",
    "title": "User Registration",
    "group": "Signup",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Registration successful</p>"
          },
          {
            "group": "200",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>Registration successful!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/users.js",
    "groupTitle": "Signup",
    "name": "PostSignup"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "User Listing",
    "group": "Users",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "users",
            "description": "<p>User Object</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Unauthorized!</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/users.js",
    "groupTitle": "Users",
    "name": "PostUsers"
  }
] });
