// Node modules
const express = require('express');

// User modules
const authenticate = require('../authenticate');
const cors = require('./cors');

// Models
const Projects = require('../models/project');

const router = express.Router();

/**
 * @api {get} * GET methods
 * @apiGroup Mock Server
 * 
 * @apiSuccess (200) {String} responseBody Sample Response
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.get('*', function(req, res, next) {
  Projects.find({})
  .then((projects) => {

    let flag = true; // Found flag
    for(i=0; i<projects.length; i++) {
      if(projects[i].name != null) {
        for(j=0; j< projects[i].apis.length; j++) {
          
          // Check if GET method is found
          if(projects[i].apis[j].url == req.url && (projects[i].apis[j].method == "GET" || projects[i].apis[j].method == "get")) {
            flag = false;
            
            if(projects[i].apis[j].optionalHeaders != null) {
              // Add headers
              headers = projects[i].apis[j].optionalHeaders.split(',');
              res.append(headers[0], headers[1]);
            }
            
            res.statusCode = 200;
            res.send(projects[i].apis[j].responseBody);
          }
        }        
      }
      else {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Not found!"});
      }
    }
    if(flag) {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Not found!"});
    }
  }, (err) => { next(err) })
  .catch((err) => { next(err) });
})

/**
 * @api {POST} * POST methods
 * @apiGroup Mock Server
 * 
 * @apiSuccess (200) {String} responseBody Sample Response
 */

.post('*', function(req, res, next) {
  Projects.find({})
  .then((projects) => {

    let flag = true; // Found flag
    for(i=0; i<projects.length; i++) {
      if(projects[i].name != null) {
        for(j=0; j< projects[i].apis.length; j++) {
          
          // Check if POST method is found
          if(projects[i].apis[j].url == req.url && (projects[i].apis[j].method == "POST" || projects[i].apis[j].method == "post")) {
            flag = false;
            
            if(projects[i].apis[j].optionalHeaders != null) {
              // Add headers
              headers = projects[i].apis[j].optionalHeaders.split(',');
              res.append(headers[0], headers[1]);
            }
            
            res.statusCode = 200;
            res.send(projects[i].apis[j].responseBody);
          }
        }        
      }
      else {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Not found!"});
      }
    }
    if(flag) {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Not found!"});
    }

  }, (err) => { next(err) })
  .catch((err) => { next(err) });
})

/**
 * @api {put} * PUT methods
 * @apiGroup Mock Server
 * 
 * @apiSuccess (200) {String} responseBody Sample Response
 */
.put('*', function(req, res, next) {
  Projects.find({})
  .then((projects) => {

    let flag = true; // Found flag
    for(i=0; i<projects.length; i++) {
      if(projects[i].name != null) {
        for(j=0; j< projects[i].apis.length; j++) {
          
          // Check if PUT method is found
          if(projects[i].apis[j].url == req.url && (projects[i].apis[j].method == "PUT" || projects[i].apis[j].method == "put")) {
            flag = false;
            
            if(projects[i].apis[j].optionalHeaders != null) {
              // Add headers
              headers = projects[i].apis[j].optionalHeaders.split(',');
              res.append(headers[0], headers[1]);
            }
            
            res.statusCode = 200;
            res.send(projects[i].apis[j].responseBody);
          }
        }        
      }
      else {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Not found!"});
      }
    }
    if(flag) {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Not found!"});
    }

  }, (err) => { next(err) })
  .catch((err) => { next(err) });
})

/**
 * @api {delete} * DELETE methods
 * @apiGroup Mock Server
 * 
 * @apiSuccess (200) {String} responseBody Sample Response
 */
.delete('*', function(req, res, next) {
  Projects.find({})
  .then((projects) => {

    let flag = true; // Found flag
    for(i=0; i<projects.length; i++) {
      if(projects[i].name != null) {
        for(j=0; j< projects[i].apis.length; j++) {
          
          // Check if DELETE method is found
          if(projects[i].apis[j].url == req.url && (projects[i].apis[j].method == "DELETE" || projects[i].apis[j].method == "delete")) {
            flag = false;
            
            if(projects[i].apis[j].optionalHeaders != null) {
              // Add headers
              headers = projects[i].apis[j].optionalHeaders.split(',');
              res.append(headers[0], headers[1]);
            }
            
            res.statusCode = 200;
            res.send(projects[i].apis[j].responseBody);
          }
        }        
      }
      else {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Not found!"});
      }
    }
    if(flag) {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Not found!"});
    }

  }, (err) => { next(err) })
  .catch((err) => { next(err) });
});

module.exports = router;