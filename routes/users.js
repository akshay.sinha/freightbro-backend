// Node modules
const express = require('express');
const passport = require('passport');

// User modules
const authenticate = require('../authenticate');
const cors = require('./cors');

// Models
const User = require('../models/user');

const router = express.Router();

/**
 * @api {post} /users User Listing
 * @apiGroup Users
 * 
 * @apiSuccess (200) {Object} users User Object
 * 
 * @apiError (401) {String} error Unauthorized!
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.get('/users', authenticate.verifyUser, function(req, res, next) {
  User.find({})
  .then((users) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(users);
  }, (err) => { next(err) })
  .catch((err) => next(err));
});


/**
 * @api {post} /signup User Registration
 * @apiGroup Signup
 * @apiParam {String} username Username
 * @apiParam {String} password Password
 * 
 * @apiSuccess (200) {Boolean} success Registration successful
 * @apiSuccess (200) {string} status  Registration successful!
 * 
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.post('/signup', function(req, res, next) {
  // Register new user
  User.register(new User({username: req.body.username}), 
    req.body.password, (err, user) => {
    if(err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'application/json');
      res.json({err: err});
    }
    else {
      user.save((err, user) => {
        if (err) {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'application/json');
          res.json({err: err});
          return;
        }
        passport.authenticate('local')(req, res, () => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({success: true, status: 'Registration Successful!'});
        }); 
      });
      
    }
  });
});

/**
 * @api {post} /login User login
 * @apiGroup Login
 * @apiParam {String} username Username
 * @apiParam {String} password Password
 * 
 * @apiSuccess (200) {Boolean} success Login successful
 * @apiSuccess (200) {JWT} token JSON Web token
 * @apiSuccess (200) {string} status  You are successfully logged in!
 * 
 * @apiError (401) {String} error Unauthorized!
 */

router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.post('/login', passport.authenticate('local'), function(req, res, next) {
  // Get JWT token
  var token = authenticate.getToken({_id: req.user._id});
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({success: true, token: token, status: 'You are successfully logged in!'});
});

module.exports = router;
