// Node modules
const express = require('express');

// User Modules
const authenticate = require('../authenticate');
const cors = require('./cors');

// Models
const Projects = require('../models/project');

const router = express.Router();


/**
 * @api {get} /project Project Details
 * @apiGroup Project
 * 
 * @apiSuccess (200) {Object} projects Projects Object
 * 
 * @apiError (401) {String} error Unauthorized!
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.get('/project', authenticate.verifyUser, function(req, res, next) {
  Projects.find({})
  .then((projects) => {
    let projectList = [];
    let temp = {};
    for(i=0; i < projects.length; i++) {
      // Stringify Arrays
      temp.viewUsers = [];
      temp.editUsers = [];

      for(j = 0;j < projects[i].viewUsers.length; j++) {
        temp.viewUsers[j] = JSON.stringify(projects[i].viewUsers[j]);
      }
      for(j = 0;j < projects[i].editUsers.length; j++) {
        temp.editUsers[j] = JSON.stringify(projects[i].editUsers[j]);
      }

      // Check if user is owner or can view or edit project
      if(JSON.stringify(projects[i].owner) == JSON.stringify(req.user._id) || temp.viewUsers.indexOf(JSON.stringify(req.user._id)) != -1 || temp.editUsers.indexOf(JSON.stringify(req.user._id)) != -1) {
        projectList.push(projects[i]);
      }
    }
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(projectList);
  }, (err) => next(err))
  .catch((err) => next(err));
})


/**
 * @api {post} /project User login
 * @apiGroup Project
 * @apiParam {String} name Project Name
 * @apiParam {String} owner Project Owner
 * 
 * @apiSuccess (200) {Object} project Project Object
 * 
 * @apiError (401) {String} error Unauthorized!
 * @apiError (401) {JSON} error Project already exists!
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.post('/project', authenticate.verifyUser, function(req, res, next) {
  Projects.findOne({name: req.body.name})
  .then((project) => {
    // If project does not exist
    if(project == null) {
      let newProject = {
        name: req.body.name,
        owner: req.user._id
      };

      // Create new project
      Projects.create(newProject)
      .then((project) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(project);
      }, (err) => { next(err) });      
    }
    else {
      res.statusCode = 401;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Project already exists!"});
    }
  }, (err) => { next(err) })
  .catch((err) => { next(err) });  
});

/**
 * @api {put} /project/:name Project Method update
 * @apiGroup Project
 * @apiParam {String} url Url
 * @apiParam {String} method Method Type
 * @apiParam {String} requestBody Sample Request 
 * @apiParam {String} responseBody Sample Response
 * @apiParam {String} optionalHeaders Header
 * 
 * @apiSuccess (200) {String} Updated Updated
 * 
 * @apiError (401) {String} error Unauthorized!
 * @apiError (404) {String} error Project not found!
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.put('/project/:name/', authenticate.verifyUser, function(req, res, next) {
  Projects.findOne({ name: req.params.name})
  .then((project) => {
    if(project != null) {
      
      // Stringify Array
      var temp = {};
      temp.editUsers = [];
      
      for(j=0;j<project.editUsers.length; j++) {
        temp.editUsers[j] = JSON.stringify(project.editUsers[j]);
      }

      // Check if user can update project
      if(JSON.stringify(project.owner) == JSON.stringify(req.user._id)  || temp.editUsers.indexOf(JSON.stringify(req.user._id)) != -1) {
        
        // Update api call
        Projects.update({'apis.url': req.body.url, 'apis.method': req.body.method}, {'$set': {'apis.$': req.body}}, function(err){
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json("Updated");
        });
      }
      else {
        res.statusCode = 401;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Unauthorized!"});        
      }
    }
    else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Project not found!"});      
    }
  }, (err) => { next(err) })
  .catch((err) => { next(err) });
});

/**
 * @api {post} /project/:name New Project Method
 * @apiGroup Project
 * @apiParam {String} url Url
 * @apiParam {String} method Method Type
 * @apiParam {String} requestBody Sample Request 
 * @apiParam {String} responseBody Sample Response
 * @apiParam {String} optionalHeaders Header
 * 
 * @apiSuccess (200) {String} Updated Updated
 * 
 * @apiError (401) {String} error Unauthorized!
 * @apiError (401) {String} error Method already exists!
 * @apiError (404) {String} error Project not found!
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.post('/project/:name/', authenticate.verifyUser, function(req, res, next) {
  Projects.findOne({ name: req.params.name})
  .then((project) => {
    if(project != null) {      
      console.log(JSON.stringify(project.owner) == JSON.stringify(req.user._id));

      // Stringify array
      var temp = {};
      temp.editUsers = [];
      for(j=0;j<project.editUsers.length; j++) {
        temp.editUsers[j] = JSON.stringify(project.editUsers[j]);
      }

      // Check if user can edit the project
      if(JSON.stringify(project.owner) == JSON.stringify(req.user._id)  || temp.editUsers.indexOf(JSON.stringify(req.user._id)) != -1) {

        //Check if method already exists
        let methodExists = false;
        for(i=0; i < project.apis.length; i++) {
          if(project.apis[i].url.toLowerCase() == req.body.url.toLowerCase() && project.apis[i].method.toLowerCase() == req.body.method.toLowerCase()) {
            methodExists = true;
            res.statusCode = 401;
            res.setHeader('Content-Type', 'application/json');
            res.json({error: "Method already exists!"}); 
            break;        
          }
        }

        if(!methodExists) {
          project.apis.push(req.body);
          project.save()
          .then((project) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(project);
          }, (err) => { next(err) });
        }        
      }
      else {
        res.statusCode = 401;
        res.setHeader('Content-Type', 'application/json');
        res.json({error: "Unauthorized!"});         
      }
    }
    else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Project not found!"});      
    }
  }, (err) => { next(err) })
  .catch((err) => { next(err) });
});


/**
 * @api {post} /adduser/:projectName New Project Method
 * @apiGroup Project
 * @apiParam {String} url Url
 * @apiParam {String} method Method Type
 * @apiParam {String} requestBody Sample Request 
 * @apiParam {String} responseBody Sample Response
 * @apiParam {String} optionalHeaders Header
 * 
 * @apiSuccess (200) {String} Updated Updated
 * 
 * @apiError (401) {String} error Unauthorized!
 * @apiError (401) {String} error Method already exists!
 * @apiError (404) {String} error Project not found!
 */
router
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200) })
.post('/adduser/:projectName', authenticate.verifyUser, function(req, res, next) {
  Projects.findOne({ name: req.params.projectName })
  .then((project) => {
    if(project != null) {
      // Add user to viewUsers/editUsers array
       if(req.body.permissions == "view") {
         project.viewUsers.push(req.body.userid);
       }
       if(req.body.permissions == "edit") {
        project.editUsers.push(req.body.userid);
       }

       if(req.body.permissions == "view" || req.body.permissions == "edit"){
        // Save project
        project.save()
        .then((project) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(project);
        }, (err) => { next(err) });
       }
       
    }
    else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({error: "Project not found!"});      
    }
  }, (err) => { next(err) })
  .catch((err) => { next(err) });
});

module.exports = router;