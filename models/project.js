var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var APISchema = new Schema({
  url: {
    type: String,
    default: '/test'
  },
  method: {
    type: String,
    default: 'GET'
  },
  requestBody: {
    type: String,
    default: 'Hello'
  },
  responseBody: {
    type: String,
    default: 'Welcome'
  },
  optionalHeaders: {
    type: String
  }
}, {
  usePushEach: true  
});

var projectSchema = new Schema({
    name: {
        type: String,        
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    viewUsers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    editUsers: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }], 
    apis: [APISchema]
}, {
  usePushEach: true  
});

module.exports = mongoose.model('Project', projectSchema);